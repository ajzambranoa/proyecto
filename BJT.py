
#from _typeshed import Self

from tkinter import messagebox


class BJT:
    # Amplificador E.C acoplado con capacitor
    #Av, beta, Rl, Vcc, Vbe = input().split()
    def __init__(self, Av, beta, Rl, Vcc, Vbe):
        self.Av = float(Av)
        self.beta = float(beta)
        self.Rl = float(Rl)
        self.Vcc = float(Vcc)
        self.Vbe = float(Vbe)

    def Obtencion_Re(self) -> float:
        paralelo = (self.Rl / 2)
        R_e = - paralelo / self.Av
        return R_e

    def Obtencion_Rac_Rdc_con_Re(self) -> list:
        R_e = self.Obtencion_Re()
        resistencias_q = []
        paralelo = (pow(self.Rl, 2))/(2 * self.Rl)
        R_ca = R_e + paralelo
        R_dc = R_e + self.Rl
        resistencias_q.append(R_ca)
        resistencias_q.append(R_dc)
        return resistencias_q

    def Obtencion_Rac_Rdc_sin_Re(self) -> list:
        R_e = self.Obtencion_Re()
        resistencias_q = []
        paralelo = (pow(self.Rl, 2))/(2 * self. Rl)
        R_ca = paralelo
        R_dc = R_e + self.Rl
        resistencias_q.append(R_ca)
        resistencias_q.append(R_dc)
        return resistencias_q

    def Maxima_escursion(self, resistencias_q: list) -> float:
        Ic_q = self.Vcc / (resistencias_q[0] + resistencias_q[1])
        return Ic_q

    def verificacion_Av_corta(self, Ic_q: float) -> bool:
        hib = self.obtener_hib(Ic_q)
        R_e = self.Obtencion_Re()
        R_e1 = R_e - hib
        if hib < R_e1-(0.1 * R_e1):
            return True
        else:
            return False

    def Obtener_Re_Av_larga(self, Ic_q: float) -> float:
        hib = self.obtener_hib(Ic_q)
        paralelo = (pow(self.Rl, 2))/(2 * self.Rl)
        R_e = (paralelo / self.Av) - hib
        return R_e

    def Obtencion_Rb(self, R_e: float) -> float:
        Rb = 0.1 * (self.beta)*(R_e)
        return Rb

    def Obtencion_Rblarga(self, R_e) -> float:
        Rb = 0.1 * (self.beta)*(R_e)
        return Rb

    def Obtencion_comparacion_Re_hib(self, Icq: float, R_e: float) -> float:
        hib = self.obtener_hib(Icq)
        Re_hib = R_e - hib
        return Re_hib

    def Obtener_Ai(self, Rb: float, R_e: float) -> float:

        Ai = (-Rb * self.Rl)/(R_e*(2*self.Rl))
        return Ai

    def obtener_hib(self, Ic_q: float) -> float:
        hib = 26 * pow(10, -3) / Ic_q
        return hib

    def verificacion_Ai_corta(self, R_e: float, R_b: float, Ic_q: float, Ai_an: float) -> bool or float:
        hib = self.obtener_hib(Ic_q)
        Ai_1 = abs(Ai_an)
        Ai = abs(((-R_b)/(R_b/self.beta + hib + R_e)) * (1/2))
        if Ai < Ai_1-(Ai_1 / 10):
            return Ai * (-1)
        else:
            return True

    def Obtener_Vceq(self, R_e: float, Icq: float) -> float:
        Vceq = self.Vcc-(self.Rl + R_e) * Icq
        return Vceq

    def Obtencion_Vbb(self, Icq: float, Rb: float, R_e: float) -> float:
        #Rb = Obtencion_Rb()
        #R_e = Obtencion_comparacion_Re_hib(Icq)
        Vbb = Icq*(R_e + Rb / self.beta) + self.Vbe
        return Vbb

    def Obtencion_R1(self, Vbb: float, Rb: float) -> float:
        R_1 = (Rb) / (1-(Vbb / self.Vcc))
        return R_1

    def Obtencion_R2(self, Vbb: float, Rb: float) -> float:
        R_2 = (self.Vcc / Vbb) * Rb
        return R_2

    def Obtencion_Ren(self, Icq: float, Rb: float, R_e: float) -> float:
        hib = 26 * pow(10, -3) / Icq
        #R_e = Obtencion_comparacion_Re_hib(Icq)
        R_en = (Rb * (hib + R_e))/((Rb / self.beta) + hib + R_e)
        return R_en

    def generarInforme(self) -> list:
        lista_final = []
        R_e = self.Obtencion_Re()
        #print("Re =", R_e)

        lista_r = self.Obtencion_Rac_Rdc_con_Re()
        #print("Rac =", lista_r[0], "", "Rdc =", lista_r[1])
        #print("Hallamos Icq")
        Icq = self.Maxima_escursion(lista_r)
        #print("Icq =", Icq)

        validoAv = self.verificacion_Av_corta(Icq)
        # print(validoAv)
        if (validoAv == False):
            lista_r = self.Obtencion_Rac_Rdc_sin_Re()
            Icq = self.Maxima_escursion(lista_r)
            R_e = self.Obtener_Re_Av_larga(Icq)
        else:
            R_b = self.Obtencion_Rb(R_e)
            #print("Rb =", R_b)
            Ai = self.Obtener_Ai(R_b, R_e)
            lista_final.append(Ai)
            #print("Ai =", Ai)
            Re_hib = self.Obtencion_comparacion_Re_hib(Icq, R_e)
            # print(Re_hib)
            R_blarga = self.Obtencion_Rblarga(Re_hib)
            validoAi = self.verificacion_Ai_corta(Re_hib, R_blarga, Icq, Ai)
            # print(validoAi)
            if (validoAi == True):
                Ai = self.Obtener_Ai(R_b)
                #print("Ai =", Ai)
            else:
                Ai = validoAi
                #print("Ai =", Ai)

            Vceq = self.Obtener_Vceq(Re_hib, Icq)
            #print("Vceq = ", Vceq)
            Vbb = self.Obtencion_Vbb(Icq, R_b, Re_hib)
            #print("Vbb = ", Vbb)
            R1 = self.Obtencion_R1(Vbb, R_blarga)
            #print("R1 =", R1)
            R2 = self.Obtencion_R2(Vbb, R_blarga)
            #print("R2 =", R2)
            Rent = self.Obtencion_Ren(Icq, R_blarga,  Re_hib)
            #print("Rent =", Rent)
            lista_final.append(R_e)
            lista_final.append(lista_r[0])
            lista_final.append(lista_r[1])
            lista_final.append(Icq)
            lista_final.append(R_b)
            lista_final.append(Ai)
            lista_final.append(Vceq)
            lista_final.append(Vbb)
            lista_final.append(R1)
            lista_final.append(R2)
            lista_final.append(Rent)
            lista_final.append(validoAi)
            lista_final.append(validoAv)
        return lista_final


#ob = BJT(-10, 200, 1000, 12, 0.7)
# ob.generarInforme()
