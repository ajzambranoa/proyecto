
from tkinter import *
from BJT import *
from functools import partial
import time


class Vista(object):
    reset_pantalla = False

    def __init__(self):
        self.raiz = Tk()
        self.raiz.title("Amplificadores")
        self.raiz.geometry("850x750")

        self.entAv = StringVar()
        self.entB = StringVar()
        self.entRl = StringVar()
        self.entVcc = StringVar()
        self.entVbe = StringVar()
        self.caja = StringVar()

        self.frame1 = Frame()
        self.frame1.config(relief="groove")
        self.frame1.config(bd="3")
        self.frame2 = Frame()
        self.frame3 = Frame()
        self.frame3.place(x=650, y=300)
        self.frame2.pack()
        self.frame1.place(x=200, y=100)

        self.labelTitulo = Label(
            self.frame2, text="Diseño de amplificadores E.C  ", font=(18), justify=CENTER)
        self.labelTitulo.grid(row=1, column=1, padx=10, pady=20, columnspan=4)

        self.labelAv = Label(self.frame1,  text="Av: ",
                             font=(18), justify=RIGHT)
        self.labelAv.grid(row=2, column=1, padx=10, pady=10, sticky="w")
        self.labelBeta = Label(self.frame1,  text="β: ",
                               font=(18), justify="right")
        self.labelBeta.grid(row=3, column=1, padx=10, pady=10, sticky="w")
        self.labelRl = Label(self.frame1,  text="RL(Ω): ",
                             font=(18), justify="right")
        self.labelRl.grid(row=4, column=1, padx=10, pady=10, sticky="w")
        self.labelVcc = Label(self.frame1,  text="Vcc: ",
                              font=(18), justify="right")
        self.labelVcc.grid(row=5, column=1, padx=10, pady=10, sticky="w")
        self.labelVbe = Label(self.frame1,  text="Vbe: ",
                              font=(18), justify="right")
        self.labelVbe.grid(row=6, column=1, padx=10, pady=10, sticky="w")
        # cajas de texto salida
        self.caja = Label(self.frame3)
        self.caja.grid(row=6, column=1,  pady=5)
        self.caja1 = Label(self.frame3)
        self.caja1.grid(row=7, column=1, padx=10, pady=5)
        self.caja2 = Label(self.frame3)
        self.caja2.grid(row=8, column=1, padx=10, pady=5)
        self.caja3 = Label(self.frame3)
        self.caja3.grid(row=9, column=1, padx=10, pady=5)
        self.caja4 = Label(self.frame3)
        self.caja4.grid(row=10, column=1, padx=10, pady=5)
        self.caja5 = Label(self.frame3)
        self.caja5.grid(row=11, column=1, padx=10, pady=5)
        self.caja6 = Label(self.frame3)
        self.caja6.grid(row=12, column=1, padx=10, pady=5)
        self.caja7 = Label(self.frame3)
        self.caja7.grid(row=13, column=1, padx=10, pady=5)
        self.caja8 = Label(self.frame3)
        self.caja8.grid(row=14, column=1, padx=10, pady=5)
        self.caja9 = Label(self.frame3)
        self.caja9.grid(row=15, column=1, padx=10, pady=5)
        self.caja10 = Label(self.frame3)
        self.caja10.grid(row=16, column=1, padx=10, pady=5)
        self.caja11 = Label(self.frame3)
        self.caja11.grid(row=17, column=1, padx=10, pady=5)
        self.caja12 = Label(self.frame3)
        self.caja12.grid(row=18, column=1, padx=10, pady=5)

        self.txtAv = Entry(self.frame1, textvariable=self.entAv)
        self.txtAv.grid(row=2, column=2, padx=10, pady=10)
        self.txtBeta = Entry(self.frame1, textvariable=self.entB)
        self.txtBeta.grid(row=3, column=2, padx=10, pady=10)
        self.txtRl = Entry(self.frame1, textvariable=self.entRl)
        self.txtRl.grid(row=4, column=2, padx=10, pady=10)
        self.txtVcc = Entry(self.frame1, textvariable=self.entVcc)
        self.txtVcc.grid(row=5, column=2, padx=10, pady=10)
        self.txtVbe = Entry(self.frame1, textvariable=self.entVbe)
        self.txtVbe.grid(row=6, column=2, padx=10, pady=10)

        # action_with_arg = partial(obtenerInforme, Av, beta, Rl, Vcc, Vbe)
        # print(Av)

        self.miImage = PhotoImage(file="transistor.png")
        self.miLabelIm = Label(
            self.raiz, image=self.miImage).place(x=100, y=380)

        self.botonEnvio = Button(self.raiz, text="Enviar",
                                 command=self.verificarEspaciosVacios).place(x=300, y=325)
        self.raiz.mainloop()

    def verificarEspaciosVacios(self):
        if self.entAv.get() == "" or self.entB.get() == "" or self.entRl.get() == "" or self.entVcc.get() == "" or self.entVbe.get() == "":

            messagebox.showerror(message="Los campos no pueden estar vacios")

        else:
            self.verificarNumeros()

    def verificarNumeros(self):
        try:
            float(self.entAv.get())
            float(self.entB.get())
            float(self.entRl.get())
            float(self.entVcc.get())
            float(self.entVbe.get())
            self.noCero()
        except ValueError:
            messagebox.showerror(
                message="Los especios deben ser llenados con números")
            self.limpiarTexto()

    def noCero(self):
        if float(self.entAv.get()) == 0 or float(self.entRl.get()) == 0 or float(self.entB.get()) == 0 or float(self.entVbe.get()) == 0 or float(self.entVcc.get()) == 0:
            messagebox.showerror(
                message="Los espacios no puede ser 0")
            self.limpiarTexto()

        else:
            self.obtenerInforme()

    def obtenerInforme(self):
        global reset_pantalla
        ob = BJT(self.entAv.get(), self.entB.get(),
                 self.entRl.get(), self.entVcc.get(), self.entVbe.get())
        lista_final = []
        lista_final = ob.generarInforme()
        self.mostarDatos(lista_final)
        self.limpiarTexto()

    def mostarDatos(self, lista_final):
        R_e = str(round(lista_final[1], 2))

        messagebox.showinfo(
            message="Para máxima transferencia de potencia se coloca Rc = Rl \n "
            + "Utilizamos la ecuación de forma corta de Av \n y suponemos hib <<<Re para encontrar Re" +
            "\n"+"Re ="+R_e+" Ω",
            title="Analisis de diseño")

        Icq = str(round(lista_final[4]*pow(10, 3), 2))
        R_ac = str(round(lista_final[2], 2))
        R_dc = str(round(lista_final[3], 2))
        if lista_final[13] == True:
            Avvalida = "Verdadero"
        else:
            Avvalida = "Falso"

        messagebox.showinfo(
            message="Se necesita encontrar hib por lo tanto \n primer hallamos Icq ,"
            + " sacamos Rac y Rdc para hallar \n máxima excursión simetrica: \n"+"Icq ="+Icq+"mA"
            + "\n"+"Rac ="+R_ac+" Ω"
            + "\n"+"Rdc ="+R_dc+" Ω")

        messagebox.showinfo(message="Hallando hib podemos saber si Re es mucho mayor que"
                            + " \n hib y por tanto verificamos si es posible seguir usando la forma corta de Av"
                            + "\n normalmente lo es si no hay un condensador de desacople"+"\n"+"¿Re es mayor que hib? "+Avvalida)

        Rb = str(round(lista_final[5], 2))
        Ai_an = str(round(lista_final[0], 2))
        Ai = str(round(lista_final[6], 2))

        if Avvalida == True:
            messagebox.showinfo(message="Se halla Rb sabiendo Re.\nForzamos a que Rb sea menor a βRe en un factor de 10"
                                + "\n"+"Rb ="+Rb)
            messagebox.showinfo(message="Intentamos calcular Ai de forma corta"+"\n"
                                + "Ai ="+Ai_an)
            messagebox.showinfo(
                message="Se calcula Ai de forma larga y se compara el mergen de error, si es muy alto se sigue con la forma larga ")

        if lista_final[12] == True:
            messagebox.showinfo(message="Ai en forma corta es :"+Ai)
        else:
            messagebox.showinfo(message="Ai en forma larga es :"+Ai)

        Vceq = str(round(lista_final[7], 2))
        Vbb = str(round(lista_final[8], 2))
        R1 = str(round(lista_final[9], 2))
        R2 = str(round(lista_final[10], 2))
        Rent = str(round(lista_final[11], 2))

        messagebox.showinfo(message="Con los parametros obtenidos anteriormente se halla Vceq , Vbb, R1 , R2 y Rent"
                            + "\n"+"Vceq ="+Vceq
                            + "\n"+"Vbb ="+Vbb
                            + "\n"+"R1 ="+R1+"Ω"
                            + "\n"+"R2 ="+R2+"Ω"
                            + "\n"+"Rent ="+Rent+"Ω"
                            + "\n"+"Rl ="+self.entRl.get()+" Ω"
                            + "\n"+"Rc ="+self.entRl.get()+" Ω")

        self.caja.config(text="R_e="+R_e+" Ω")
        self.caja1.config(text="Rac ="+R_ac+" Ω")
        self.caja2.config(text="Rdc =" + R_dc+" Ω")
        self.caja3.config(text="Icq ="+Icq+"mA")
        self.caja4.config(text="Rb =" + Rb+" Ω")
        self.caja5.config(text="Ai =" + Ai)
        self.caja6.config(text="Vceq ="+Vceq+"V")
        self.caja7.config(text="Vbb ="+Vbb+"V")
        self.caja8.config(text="R1 ="+R1+" Ω")
        self.caja9.config(text="R2 ="+R2+" Ω")
        self.caja10.config(text="Rent ="+Rent+" Ω")
        self.caja11.config(text="Rl ="+self.entRl.get()+" Ω")
        self.caja12.config(text="Rc ="+self.entRl.get()+" Ω")

    def limpiarTexto(self):
        self.entAv.set("")
        self.entB.set("")
        self.entRl.set("")
        self.entVcc.set("")
        self.entVbe.set("")


vista = Vista()
