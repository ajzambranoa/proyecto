import numpy as np


class FET:
    def __init__(self, Av, Rent, Rl, Idss, Vp, Vdd):
        self.Av = float(Av)
        self.Rent = float(Rent)
        self.Rl = float(Rl)
        self.Idss = float(Idss)
        self.Vp = float(Vp)
        self.Vdd = float(Vdd)

    def obtencionPuntoQ(self) -> list:
        listaParametros = []
        Idq = self.Idss/2
        listaParametros.append(Idq)
        Vgsq = 0.3*self.Vp
        listaParametros.append(Vgsq)
        Vdsq = self.Vdd/2
        listaParametros.append(Vdsq)
        return listaParametros

    def obtencionGm(self) -> float:
        gm = 1.42*(self.Idss/abs(self.Vp))
        gmInv = 1 / gm
        return gmInv

    def obtencionK(self, Vdsq, Idq) -> float:
        k = (self.Vdd-Vdsq)/(Idq) * pow(10, 3)
        return k

    def obtencionRd(self, k, gmInv) -> float:
        A = 1
        B = -k+self.Rl-gmInv-(self.Rl/self.Av)
        C = (self.Rl*gmInv)+self.Rl*k
        Rd_p = (-B+np.sqrt(pow(B, 2)-4*A*-C))/(2*A)
        Rd_n = (-B-np.sqrt(pow(B, 2)-4*A*-C))/(2*A)
        if Rd_p > 0 and Rd_n > 0 and Rd_n == Rd_p:
            return Rd_p
        elif Rd_p > 0 and Rd_n <= 0:
            return Rd_p
        elif Rd_n > 0 and Rd_p <= 0:
            return Rd_n

    def obtenerRs(self, Vdsq, Idq, Rd) -> float:
        Rs = ((self.Vdd-Vdsq)/(Idq*pow(10, -3)))-Rd
        return Rs

    def obtenerVgg(self, Vgsq, Idq, Rs) -> float:
        Vgg = Vgsq+(Idq * pow(10, -3))*Rs
        return Vgg

    def obtencioR1(self, Vgg) -> float:
        R1 = self.Rent / (1-Vgg/self.Vdd)
        return R1

    def obtencionR2(self, Vgg) -> float:
        R2 = (self.Rent*self.Vdd)/Vgg
        return R2

    def polarizacionVgg(Vgg) -> bool:
        if Vgg < 0:
            return False
        else:
            return True

    def generarInforme(self):
        listaParametros = self.obtencionPuntoQ()
        Idq = listaParametros[0]
        Vgsq = listaParametros[1]
        Vdsq = listaParametros[2]
        #print("Idq:", Idq)
        #print("Vgsq: ", Vgsq)
        #print("Vdsq: ", Vdsq)
        gmInv = self.obtencionGm()
        #print("1/gm :", round(gmInv*pow(10, 3), 1), "Ω")
        k = self.obtencionK(Vdsq, Idq)
        #print("k:", round(k, 1))
        Rd = self.obtencionRd(k, gmInv)
        #print("Rd: ", round(Rd * pow(10, -3), 1), "kΩ")
        Rs = self.obtenerRs(Vdsq, Idq, Rd)
        #print("Rs: ", round(Rs, 1))
        Vgg = self.obtenerVgg(Vgsq, Idq, Rs)
        #print("Vgg :", Vgg)
        R1 = self.obtencioR1(Vgg)
        R2 = self.obtencionR2(Vgg)
        #print("R1 = ", R1)
        #print("R2 =", R2)
        lista_final = []
        lista_final.append(Idq)
        lista_final.append(Vgsq)
        lista_final.append(Vdsq)
        lista_final.append(gmInv)
        lista_final.append(k)
        lista_final.append(Rd)
        lista_final.append(Rs)
        lista_final.append(Vgg)
        lista_final.append(R1)
        lista_final.append(R2)

        return lista_final


#ob = FET(-4, 100000, 20000, 6.67, -3.33, 20)
# ob.generarInforme()
