
from tkinter import *
from FET import *
from functools import partial
import time
from tkinter import messagebox


class Vista2(object):
    reset_pantalla = False

    def __init__(self):
        self.raiz = Tk()
        self.raiz.title("Amplificadores")
        self.raiz.geometry("850x750")

        self.entAv = StringVar()
        self.entRent = StringVar()
        self.entRl = StringVar()
        self.entIdss = StringVar()
        self.entVp = StringVar()
        self.entVdd = StringVar()
        self.caja = StringVar()

        self.frame1 = Frame()
        self.frame1.config(relief="groove")
        self.frame1.config(bd="3")
        self.frame2 = Frame()
        self.frame3 = Frame()
        self.frame3.place(x=650, y=300)
        self.frame2.pack()
        self.frame1.place(x=200, y=100)

        self.labelTitulo = Label(
            self.frame2, text="Diseño de amplificadores F.C  ", font=(18), justify=CENTER)
        self.labelTitulo.grid(row=1, column=1, padx=10, pady=20, columnspan=4)

        self.labelAv = Label(self.frame1,  text="Av: ",
                             font=(18), justify=RIGHT)
        self.labelAv.grid(row=2, column=1, padx=10, pady=10, sticky="w")
        self.labelRent = Label(self.frame1,  text="Rent: ",
                               font=(18), justify="right")
        self.labelRent.grid(row=3, column=1, padx=10, pady=10, sticky="w")
        self.labelRl = Label(self.frame1,  text="RL(Ω): ",
                             font=(18), justify="right")
        self.labelRl.grid(row=4, column=1, padx=10, pady=10, sticky="w")
        self.labelIdss = Label(self.frame1,  text="Idss(mA): ",
                               font=(18), justify="right")
        self.labelIdss.grid(row=5, column=1, padx=10, pady=10, sticky="w")
        self.labelVp = Label(self.frame1,  text="Vp: ",
                             font=(18), justify="right")
        self.labelVp.grid(row=6, column=1, padx=10, pady=10, sticky="w")
        self.labelVdd = Label(self.frame1,  text="Vdd: ",
                              font=(18), justify="right")
        self.labelVdd.grid(row=7, column=1, padx=10, pady=10, sticky="w")
        # cajas de texto salida
        self.caja = Label(self.frame3)
        self.caja.grid(row=6, column=1,  pady=5)
        self.caja1 = Label(self.frame3)
        self.caja1.grid(row=7, column=1, padx=10, pady=5)
        self.caja2 = Label(self.frame3)
        self.caja2.grid(row=8, column=1, padx=10, pady=5)
        self.caja3 = Label(self.frame3)
        self.caja3.grid(row=9, column=1, padx=10, pady=5)
        self.caja4 = Label(self.frame3)
        self.caja4.grid(row=10, column=1, padx=10, pady=5)
        self.caja5 = Label(self.frame3)
        self.caja5.grid(row=11, column=1, padx=10, pady=5)
        self.caja6 = Label(self.frame3)
        self.caja6.grid(row=12, column=1, padx=10, pady=5)
        self.caja7 = Label(self.frame3)
        self.caja7.grid(row=13, column=1, padx=10, pady=5)
        self.caja8 = Label(self.frame3)
        self.caja8.grid(row=14, column=1, padx=10, pady=5)
        self.caja9 = Label(self.frame3)
        self.caja9.grid(row=15, column=1, padx=10, pady=5)

        self.txtAv = Entry(self.frame1, textvariable=self.entAv)
        self.txtAv.grid(row=2, column=2, padx=10, pady=10)
        self.txtRent = Entry(self.frame1, textvariable=self.entRent)
        self.txtRent.grid(row=3, column=2, padx=10, pady=10)
        self.txtRl = Entry(self.frame1, textvariable=self.entRl)
        self.txtRl.grid(row=4, column=2, padx=10, pady=10)
        self.txtIdss = Entry(self.frame1, textvariable=self.entIdss)
        self.txtIdss.grid(row=5, column=2, padx=10, pady=10)
        self.txtVp = Entry(self.frame1, textvariable=self.entVp)
        self.txtVp.grid(row=6, column=2, padx=10, pady=10)
        self.txtVdd = Entry(self.frame1, textvariable=self.entVdd)
        self.txtVdd.grid(row=7, column=2, padx=10, pady=10)

        self.miImage = PhotoImage(file="transistorF.png")
        self.miLabelIm = Label(
            self.raiz, image=self.miImage).place(x=100, y=380)

        self.botonEnvio = Button(self.raiz, text="Enviar",
                                 command=self.verificarEspaciosVacios).place(x=500, y=300)
        self.raiz.mainloop()

    def verificarEspaciosVacios(self):
        if self.entAv.get() == "" or self.entRent.get() == "" or self.entRl.get() == "" or self.entIdss.get() == "" or self.entVp.get() == "" or self.entVdd.get() == "":

            messagebox.showerror(message="Los campos no pueden estar vacios")

        else:
            self.verificarNumeros()

    def verificarNumeros(self):
        try:
            float(self.entAv.get())
            float(self.entRent.get())
            float(self.entRl.get())
            float(self.entIdss.get())
            float(self.entVp.get())
            float(self.entVdd.get())
            self.noCero()
        except ValueError:
            messagebox.showerror(
                message="Los especios deben ser llenados con números")
            self.limpiarTexto()

    def noCero(self):
        if float(self.entAv.get()) == 0 or float(self.entRl.get()) == 0 or float(self.entVdd.get()) == 0 or float(self.entVp.get()) == 0 or float(self.entIdss.get()) == 0 or float(self.entRent.get()) == 0:

            messagebox.showerror(
                message="Los espacios no puede ser 0")

            self.limpiarTexto()

        else:
            self.obtenerInforme()

    def obtenerInforme(self):
        global reset_pantalla
        ob = FET(self.entAv.get(), self.entRent.get(),
                 self.entRl.get(), self.entIdss.get(), self.entVp.get(), self.entVdd.get())
        lista_final = []
        lista_final = ob.generarInforme()
        self.mostarDatos(lista_final)
        self.limpiarTexto()

    def mostarDatos(self, lista_final):
        Idq = str(round(lista_final[0], 2))
        Vgsq = str(round(lista_final[1], 2))
        Vdsq = str(round(lista_final[2], 2))
        gmInv = str(round(lista_final[3]*pow(10, 3), 2))

        messagebox.showinfo(
            message="Se selecciona un punto Q en la porción más lineal \n de las curvas caracteristicas del JFet \n "
            + "Esto identifica Vdsq, Vgsq, Idq y gm. \n En este caso los hallamos a partir de parametros dados"
            + "\n Idq = " + Idq
            + "\n Vgsq ="+Vgsq
            + "\n Vdsq ="+Vdsq
            + "\n 1/gm ="+gmInv,
            title="Analisis de diseño")

        K = str(round(lista_final[4]*pow(10, -3), 2))
        Rd = str(round(lista_final[5], 2))
        messagebox.showinfo(
            message="Hallamos k : \n la cual es Rd +Rs "
            + "\n"+"K ="+K+" kΩ")

        messagebox.showinfo(message="Utilizando la ecuación de ganancia en c.a,"
                            + " \n se obtiene"
                            + "\n " + "una ecuación cuadratica"
                            + "\n"+"tomamos la raiz positiva"
                            + "\n"+"Rd ="+Rd+" kΩ")

        Rs = str(round(lista_final[6], 2))

        messagebox.showinfo(message="Si Rd es menor que K ,\n entonces podemos hallar Rs"
                            + "\n Rs ="+Rs)

        Vgg = str(round(lista_final[7], 2))
        R1 = str(round(lista_final[8], 2))
        R2 = str(round(lista_final[9], 2))

        messagebox.showinfo(message="Si Vgg es positivo entonces,\n podemos hallar R1 y R2 "
                            + "\n Vgg =" + Vgg
                            + "\n R1 =" + R1
                            + "\n R2 ="+R2)

        self.caja.config(text="Icq="+Idq+"mA")
        self.caja1.config(text="Vgsq="+Vgsq+"V")
        self.caja2.config(text="Vdsq =" + Vdsq+"V")
        self.caja3.config(text="1/ gm  ="+gmInv+"Ω")
        self.caja4.config(text="K =" + K+" Ω")
        self.caja5.config(text="Rd =" + Rd+"kΩ")
        self.caja6.config(text="Vgg ="+Vgg+"V")
        self.caja7.config(text="R1 ="+R1+"Ω")
        self.caja8.config(text="R2 ="+R2+" Ω")
        self.caja9.config(text="Rs ="+Rs+" Ω")

    def limpiarTexto(self):
        self.entAv.set("")
        self.entRent.set("")
        self.entRl.set("")
        self.entIdss.set("")
        self.entVp.set("")
        self.entVdd.set("")


vista = Vista2()
